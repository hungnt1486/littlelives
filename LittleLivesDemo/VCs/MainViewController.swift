//
//  MainViewController.swift
//  LittleLivesDemo
//
//  Created by Hao Le on 19/10/2023.
//

import UIKit

class MainViewController: BaseViewController {
    
    var tabs = [
           ViewPagerTab(title: "Apples", image: nil),
           ViewPagerTab(title: "Cars", image: nil),
           ViewPagerTab(title: "Bitcoin", image: nil),
       ]
    
    var viewPager: ViewPagerController!
    var options: ViewPagerOptions!
    
    var apples = ApplesViewController()
    var bitcoin = BitcoinViewController()
    var cars = CarsViewController()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "News"
        print("thanh cong")
        setupPageView()

    }
    
    private func setupPageView() {
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        print("self.view.bounds.height = \(self.view.bounds.height)")
        options = ViewPagerOptions(viewPagerWithFrame: UIScreen.main.bounds)
        options.tabViewHeight = 50.0
        options.tabType = ViewPagerTabType.basic
        options.tabViewTextFont = UIFont.systemFont(ofSize: 15)
        options.tabViewPaddingLeft = 25
        options.tabViewPaddingRight = 25
        options.isTabHighlightAvailable = true
        viewPager = ViewPagerController()
        viewPager.options = options
        viewPager.dataSource = self
        
        
        self.addChild(viewPager)
        self.view.addSubview(viewPager.view)
        viewPager.didMove(toParent: self)
    }
}

extension MainViewController: ViewPagerControllerDataSource {
    func numberOfPages() -> Int {
        return tabs.count
    }
    
    func viewControllerAtPosition(position:Int) -> UIViewController {
        switch position {
        case 0:
            return apples
        case 1:
            return cars
        case 2:
            return bitcoin
        default:
            return apples
        }
    }
    
    func tabsForPages() -> [ViewPagerTab] {
        return tabs
    }
    
    func startViewPagerAtIndex() -> Int {
        return 0
    }
}
