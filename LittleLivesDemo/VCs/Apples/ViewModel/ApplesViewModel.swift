//
//  ApplesViewModel.swift
//  LittleLivesDemo
//
//  Created by Hao Le on 19/10/2023.
//

import RxSwift
import Action

class ApplesViewModel: BaseViewModelMVVM {
    let appleNewsUseCase = LittleLivesUseCaseImpl()
    let appleNewsEvent = PublishSubject<Void>()
    private lazy var appleNewsAction = makeAppleNewsAction()
    private (set) var listNews: [NewsResponse] = []
    private let strApple = "apple"
    override init() {
        super.init()
        configAppleNewsAction()
    }
    
    private func makeAppleNewsAction() -> Action<Void, [NewsResponse]> {
        return Action<Void, [NewsResponse]> { [weak self] in
            guard let self = self else { return Observable.empty() }
            return self.appleNewsUseCase.getAppleNews()
        }
    }
    
    private func configAppleNewsAction() {
        appleNewsAction
            .elements
            .subscribe(onNext: { [weak self] newsResponse in
                Utils.sharedInstance.saveDataToFile(name: self!.strApple, listNews: newsResponse)
                self?.listNews = newsResponse
                self?.appleNewsEvent.onNext(())
            })
            .disposed(by: disposeBag)
        
        appleNewsAction
            .executing
            .subscribe(onNext: { [weak self] isLoading in
                self?.loadingEvent.onNext(isLoading)
            })
            .disposed(by: disposeBag)
        
        appleNewsAction
            .errors
            .apiError
            .subscribe(onNext: { [weak self] apiError in
                if apiError.localizedDescription == "network_down" {
                    let list = Utils.sharedInstance.loadFileJson(name: self!.strApple)
                    self?.listNews = list
                }
                self?.errorEvent.onNext(apiError)
            })
            .disposed(by: disposeBag)
    }
    
    func getAppleNews() {
        appleNewsAction.execute()
    }
}



