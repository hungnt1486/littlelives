//
//  LittleLivesTableViewCell.swift
//  LittleLivesDemo
//
//  Created by Hao Le on 19/10/2023.
//

import UIKit
import SDWebImage

class LittleLivesTableViewCell: UITableViewCell {

    @IBOutlet private weak var img: UIImageView!
    @IBOutlet private weak var lbTitle: UILabel!
    @IBOutlet private weak var lbTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func bindData(model: NewsResponse) {
        lbTitle.text = model.title
        lbTime.text = model.publishedAt
        let str = model.urlToImage?.replacingOccurrences(of: " ", with: "%20")
        img.sd_setImage(with: URL.init(string: str ?? ""))
    }
    
}
