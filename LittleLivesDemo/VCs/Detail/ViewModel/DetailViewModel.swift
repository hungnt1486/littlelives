//
//  DetailViewModel.swift
//  LittleLivesDemo
//
//  Created by Hao Le on 20/10/2023.
//

import RxSwift

class DetailViewModel: BaseViewModelMVVM {
    private (set) var newsResponse: NewsResponse?
    
    init(_ selectedItem: NewsResponse?) {
        super.init()
        newsResponse = selectedItem
    }
}
