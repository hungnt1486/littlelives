//
//  DetailViewController.swift
//  LittleLivesDemo
//
//  Created by Hao Le on 20/10/2023.
//

import UIKit
import SDWebImage

class DetailViewControllerBuilder {
    static func build(_ selectedItem: NewsResponse? = nil) -> DetailViewController {
        let detail = DetailViewController(selectedItem)
        return detail
    }
}

class DetailViewController: BaseViewController {

    @IBOutlet private weak var img: UIImageView!
    @IBOutlet private weak var lbTitle: UILabel!
    @IBOutlet private weak var lbContent: UILabel!
    let viewModel: DetailViewModel
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bindingData(viewModel: viewModel)
    }
    
    init(_ selectedItem: NewsResponse?) {
        viewModel = DetailViewModel(selectedItem)
        super.init(nibName: "DetailViewController", bundle: nil)
    }
    
    private func bindingData(viewModel: DetailViewModel) {
        if let item = viewModel.newsResponse {
            lbTitle.text = item.title
            lbContent.text = item.description
            let str = item.urlToImage?.replacingOccurrences(of: " ", with: "%20")
            img.sd_setImage(with: URL.init(string: str ?? ""))
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
