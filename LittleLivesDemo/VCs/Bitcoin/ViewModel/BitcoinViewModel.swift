//
//  BitcoinViewModel.swift
//  LittleLivesDemo
//
//  Created by Hao Le on 19/10/2023.
//

import RxSwift
import Action

class BitcoinViewModel: BaseViewModelMVVM {
    let bitcoinNewsUseCase = LittleLivesUseCaseImpl()
    let bitcoinNewsEvent = PublishSubject<Void>()
    private lazy var bitcoinNewsAction = makebitcoinNewsAction()
    private (set) var listNews: [NewsResponse] = []
    private let strBitcoin = "bitcoin"
    override init() {
        super.init()
        configbitcoinNewsAction()
    }
    
    private func makebitcoinNewsAction() -> Action<Void, [NewsResponse]> {
        return Action<Void, [NewsResponse]> { [weak self] in
            guard let self = self else { return Observable.empty() }
            return self.bitcoinNewsUseCase.getBitcoinNews()
        }
    }
    
    private func configbitcoinNewsAction() {
        bitcoinNewsAction
            .elements
            .subscribe(onNext: { [weak self] newsResponse in
                Utils.sharedInstance.saveDataToFile(name: self!.strBitcoin, listNews: newsResponse)
                self?.listNews = newsResponse
                self?.bitcoinNewsEvent.onNext(())
            })
            .disposed(by: disposeBag)
        
        bitcoinNewsAction
            .executing
            .subscribe(onNext: { [weak self] isLoading in
                self?.loadingEvent.onNext(isLoading)
            })
            .disposed(by: disposeBag)
        
        bitcoinNewsAction
            .errors
            .apiError
            .subscribe(onNext: { [weak self] apiError in
                if apiError.localizedDescription == "network_down" {
                    let list = Utils.sharedInstance.loadFileJson(name: self!.strBitcoin)
                    self?.listNews = list
                }
                self?.errorEvent.onNext(apiError)
            })
            .disposed(by: disposeBag)
    }
    
    func getBitcoinNews() {
        bitcoinNewsAction.execute()
    }
}



