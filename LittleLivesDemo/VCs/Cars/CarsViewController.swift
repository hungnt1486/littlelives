//
//  CarsViewController.swift
//  LittleLivesDemo
//
//  Created by Hao Le on 19/10/2023.
//

import UIKit
import RxSwift

class CarsViewController: BaseViewController {

    @IBOutlet private weak var tbv: UITableView!
    
    private let viewModel = CarsViewModel()
    let disposedBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
        bindingEvents()
        viewModel.getCarNews()
    }
    
    private func setupTableView() {
        tbv.register(UINib(nibName: "LittleLivesTableViewCell", bundle: nil), forCellReuseIdentifier: "LittleLivesTableViewCell")
        tbv.dataSource = self
        tbv.delegate = self
        tbv.separatorColor = .clear
        tbv.separatorInset = .zero
        tbv.separatorStyle = .none
        tbv.estimatedRowHeight = 100
        tbv.rowHeight = UITableView.automaticDimension
        tbv.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
    }
    
    private func bindingEvents() {
        viewModel.carNewsEvent
            .observe(on: MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] in
                self?.tbv.reloadData()
            })
            .disposed(by: disposedBag)
        viewModel
            .loadingEvent
            .observe(on: MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] isLoading in
                self?.shouldShowProgress(should: isLoading)
            })
            .disposed(by: disposedBag)
        
        viewModel
            .errorEvent
            .observe(on: MainScheduler.asyncInstance)
            .subscribe(onNext: { [unowned self] error in
                if error.localizedDescription == "network_down" {
                    Utils.sharedInstance.showAlertView(message: error.localizedDescription, vc: self)
                    self.tbv.reloadData()
                } else {
                    Utils.sharedInstance.showAlertView(message: error.localizedDescription, vc: self)
                }
            })
            .disposed(by: disposedBag)
    }
}

// MARK: UITableViewDelegate & DataSource
extension CarsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.listNews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LittleLivesTableViewCell") as! LittleLivesTableViewCell
        let item = viewModel.listNews[indexPath.row]
        cell.bindData(model: item)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = viewModel.listNews[indexPath.row]
        let builder = DetailViewControllerBuilder.build(item)
        self.navigationController?.pushViewController(builder, animated: true)
    }
    
    
}
