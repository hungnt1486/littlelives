//
//  CarsViewModel.swift
//  LittleLivesDemo
//
//  Created by Hao Le on 19/10/2023.
//

import RxSwift
import Action

class CarsViewModel: BaseViewModelMVVM {
    let carNewsUseCase = LittleLivesUseCaseImpl()
    let carNewsEvent = PublishSubject<Void>()
    private lazy var carNewsAction = makecarNewsAction()
    private (set) var listNews: [NewsResponse] = []
    private let strCar = "car"
    override init() {
        super.init()
        configcarNewsAction()
    }
    
    private func makecarNewsAction() -> Action<Void, [NewsResponse]> {
        return Action<Void, [NewsResponse]> { [weak self] in
            guard let self = self else { return Observable.empty() }
            return self.carNewsUseCase.getCarNews()
        }
    }
    
    private func configcarNewsAction() {
        carNewsAction
            .elements
            .subscribe(onNext: { [weak self] newsResponse in
                Utils.sharedInstance.saveDataToFile(name: self!.strCar, listNews: newsResponse)
                self?.listNews = newsResponse
                self?.carNewsEvent.onNext(())
            })
            .disposed(by: disposeBag)
        
        carNewsAction
            .executing
            .subscribe(onNext: { [weak self] isLoading in
                self?.loadingEvent.onNext(isLoading)
            })
            .disposed(by: disposeBag)
        
        carNewsAction
            .errors
            .apiError
            .subscribe(onNext: { [weak self] apiError in
                if apiError.localizedDescription == "network_down" {
                    let list = Utils.sharedInstance.loadFileJson(name: self!.strCar)
                    self?.listNews = list
                }
                self?.errorEvent.onNext(apiError)
            })
            .disposed(by: disposeBag)
    }
    
    func getCarNews() {
        carNewsAction.execute()
    }
}



