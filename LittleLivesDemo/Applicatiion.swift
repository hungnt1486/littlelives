//
//  Applicatiion.swift
//  LittleLivesDemo
//
//  Created by Hao Le on 19/10/2023.
//

import UIKit

class Application {
    static let sharedInstance = Application()
    private var window: UIWindow?
    private init() {

    }
    
    func start(_ window: UIWindow) {
        self.window = window
        self.window?.makeKeyAndVisible()
        configureMainView()
    }
    
    private func configureMainView() {
        toGettingStart()
    }
    
    private func toGettingStart() {
        let main = MainViewController()
        let nav = UINavigationController(rootViewController: main)
        window?.rootViewController = nav
        window?.makeKeyAndVisible()
    }
}
