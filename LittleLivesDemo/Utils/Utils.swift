//
//  Utils.swift
//  LittleLivesDemo
//
//  Created by Hao Le on 20/10/2023.
//

import UIKit
import Alamofire

class Utils: NSObject {
    static let sharedInstance = Utils()
    
    func saveDataToFile(name: String, listNews: [NewsResponse]){
        let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let documentsDirectoryPath = NSURL(string: documentsDirectoryPathString)!

        let jsonFilePath = documentsDirectoryPath.appendingPathComponent("\(name).json")
        let fileManager = FileManager.default

        // creating a .json file in the Documents folder
        let created = fileManager.createFile(atPath: jsonFilePath?.absoluteString ?? "", contents: nil, attributes: nil)
        if created {
            print("File created ")
        } else {
            print("Couldn't create file for some reason")
        }
        
        var strJson = [[String: String?]]()
        
        for item in listNews {
           let dict =  [
                "author": item.author,
                "title": item.title,
                "description": item.description,
                "url": item.url,
                "urlToImage": item.urlToImage,
                "publishedAt": item.publishedAt,
                "content": item.content
            ]
            strJson.append(dict)
        }

        // creating JSON out of the above array
        var jsonData: NSData!
        do {
            jsonData = try JSONSerialization.data(withJSONObject: strJson, options: JSONSerialization.WritingOptions()) as NSData

        } catch let error as NSError {
            print("Array to JSON conversion failed: \(error.localizedDescription)")
        }

        // Write that JSON to the file created earlier
        do {
            let file = try FileHandle(forWritingTo: jsonFilePath!)
            file.write(jsonData as Data)
//            print("JSON data was written to teh file successfully!")
        } catch let error as NSError {
            print("Couldn't write to file: \(error.localizedDescription)")
        }
    }
    
    func loadFileJson(name: String) -> [NewsResponse] {
        var listNews: [NewsResponse] = []
        
        let documentsPath = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        let textFileURL = documentsPath.appendingPathComponent("\(name).json")
        let fileURLString = textFileURL?.path
        if FileManager.default.fileExists(atPath: (fileURLString)!){
            print("success")
        }
        do {
            let data = try Data(contentsOf: textFileURL!, options: [])
            let centralArray = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] ?? []
            for item in centralArray {
                let news = NewsResponse(
                    author: item["author"] as? String,
                    title: item["title"] as? String,
                    description: item["description"] as? String,
                    url: item["url"] as? String,
                    urlToImage: item["urlToImage"] as? String,
                    publishedAt: item["publishedAt"] as? String,
                    content: item["content"] as? String
                )
                listNews.append(news)
            }
        } catch let error {
            print(error.localizedDescription)
        }
        return listNews
    }
    
    func showAlertView(message: String, vc: UIViewController, showAlerCallback: ((_ message: String) -> Void)? = nil) -> Void {
        let alert = UIAlertController.init(title: "Title", message: message, preferredStyle: UIAlertController.Style.alert)
        let actionOk = UIAlertAction.init(title: "Ok", style: .default) { (action) in
            switch action.style {
                
            case .default:
                if let callback = showAlerCallback {
                    callback(message)
                }
            case .cancel:
                break
            case .destructive:
                break
            @unknown default:
                break
            }
            vc.dismiss(animated: true, completion: nil)
        }
        alert.addAction(actionOk)
        vc.present(alert, animated: true, completion: nil)
    }
}
