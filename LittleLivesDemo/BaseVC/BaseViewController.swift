//
//  BaseViewController.swift
//  ScanText
//
//  Created by Hao Le on 19/10/2023.
//

import UIKit
import ProgressHUD

class BaseViewController: UIViewController {
    
    func shouldShowProgress(should: Bool) {
        if should {
            ProgressHUD.show()
        } else {
            ProgressHUD.dismiss()
        }
    }
}
