//
//  BaseViewModel.swift
//  FC
//
//  Created by DEV on 06/10/2022.

//

import Foundation
import RxRelay
import RxSwift

class BaseViewModelMVVM {
    let disposeBag = DisposeBag()
    let loadingEvent = PublishSubject<Bool>()
    let errorEvent = PublishSubject<APIError>()
}
