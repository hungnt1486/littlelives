//
//  LittleLivesRepositories.swift
//  LittleLivesDemo
//
//  Created by Hao Le on 19/10/2023.
//

import RxSwift

protocol LittleLivesRepository {
    func getAppleNews() -> Observable<[NewsResponse]>
    func getCarNews() -> Observable<[NewsResponse]>
    func getBitcoinNews() -> Observable<[NewsResponse]>
}

struct LittleLivesRepositoryImpl: LittleLivesRepository {
    
    let service = LittleLivesServiceImpl()
    
    func getAppleNews() -> Observable<[NewsResponse]> {
        return service.getAppleNews()
    }
    
    func getCarNews() -> Observable<[NewsResponse]> {
        return service.getCarNews()
    }
    
    func getBitcoinNews() -> Observable<[NewsResponse]> {
        return service.getBitcoinNews()
    }
}
