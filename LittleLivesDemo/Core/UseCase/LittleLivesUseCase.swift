//
//  LittleLivesUseCase.swift
//  LittleLivesDemo
//
//  Created by Hao Le on 19/10/2023.
//

import RxSwift

protocol LittleLivesUseCase {
    func getAppleNews() -> Observable<[NewsResponse]>
    func getCarNews() -> Observable<[NewsResponse]>
    func getBitcoinNews() -> Observable<[NewsResponse]>
}

struct LittleLivesUseCaseImpl: LittleLivesUseCase {
    let repo = LittleLivesRepositoryImpl()
    func getAppleNews() -> Observable<[NewsResponse]> {
        return repo.getAppleNews()
    }
    
    func getCarNews() -> Observable<[NewsResponse]> {
        return repo.getCarNews()
    }
    
    func getBitcoinNews() -> Observable<[NewsResponse]> {
        return repo.getBitcoinNews()
    }
    
}
