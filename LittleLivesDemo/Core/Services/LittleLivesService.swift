//
//  LittleLivesService.swift
//  LittleLivesDemo
//
//  Created by Hao Le on 19/10/2023.
//

import RxSwift
import Alamofire

protocol LittleLivesService {
    func getAppleNews() -> Observable<[NewsResponse]>
    func getCarNews() -> Observable<[NewsResponse]>
    func getBitcoinNews() -> Observable<[NewsResponse]>
}

struct LittleLivesServiceImpl: LittleLivesService {
    func getAppleNews() -> Observable<[NewsResponse]> {
        return LittleLivesAPIEndpoint.getAppleNews.call()
    }
    
    func getCarNews() -> Observable<[NewsResponse]> {
        return LittleLivesAPIEndpoint.getCarNews.call()
    }
    
    func getBitcoinNews() -> Observable<[NewsResponse]> {
        return LittleLivesAPIEndpoint.getBitcoinNews.call()
    }
}

enum LittleLivesAPIEndpoint {
    case getAppleNews
    case getCarNews
    case getBitcoinNews
}

extension LittleLivesAPIEndpoint: APICall {
    
    var domain: AppEnvironment.LittleLivesDomain {
        return .littleLivesService
    }
    
    var path: String {
        switch(self) {
//        case .requestOTPCode:
//            return "/authenticate/request_code"
//        case .verifyOTPCode:
//            return "/authenticate/verify_code"
//        case .login:
//            return "/authenticate/login"
        case .getAppleNews:
            return "&q=apple"
        case .getCarNews:
            return "&q=car"
        case .getBitcoinNews:
            return "&q=bitcoin"
        }
    }
    
    var method: Alamofire.HTTPMethod {
        switch(self) {
        case .getAppleNews, .getCarNews, .getBitcoinNews:
            return .get
        }
    }
    
    var urlParams: [String : Any]? {
        switch(self) {
        default:
            return nil
        }
    }
    var paramEncoding: ParameterEncoding {
        switch(self) {
        default:
            return URLEncoding.queryString
        }
    }
    
    var headers: HTTPHeaders {
        let headerFields: HTTPHeaders = [
        ]
        return headerFields
    }
}

