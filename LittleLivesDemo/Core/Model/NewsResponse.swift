//
//  NewsResponse.swift
//  LittleLivesDemo
//
//  Created by Hao Le on 19/10/2023.
//

import Foundation

struct NewsResponse: Decodable {
    var author: String?
    var title: String?
    var description: String?
    var url: String?
    var urlToImage: String?
    var publishedAt: String?
    var content: String?
}
