//
//  AppEnvironment.swift
//  FUTA
//
//  Created by mac on 19/05/2022.

//

import Foundation

struct AppEnvironment {
    
    enum LittleLivesDomain: CustomStringConvertible {
        case littleLivesService
        
        var description: String {
            switch self {
            case .littleLivesService:
                #if DEBUG
                    return "http://newsapi.org/v2/everything?apiKey=7e968e78523641e0acd86c84af4cb6f2&from=2023-10-18&sortBy=publishedAt"
                #else
                    return "http://newsapi.org/v2/everything?apiKey=7e968e78523641e0acd86c84af4cb6f2&from=2023-10-18&sortBy=publishedAt"
                #endif
            }
        }
    }
    
    
}
