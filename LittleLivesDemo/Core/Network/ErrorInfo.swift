//
//  ErrorInfo.swift
//  FUTA
//
//  Created by mac on 19/05/2022.

//

import Foundation
import RxSwift
import Action

class ErrorInfo: Error ,Decodable {
    var status: Int = 0
    var message: String = ""
    
    enum CodingKeys: String, CodingKey {
        case message
        case status
    }
    
    init(data: Data?, statusCode: Int) {
        self.status = statusCode
        guard let data = data,
              let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {
            return
        }
        if let errorDescription = json["message"] as? String {
            self.message = errorDescription
        }
    }
}

extension ObservableType where Element == ActionError {
    
    var apiError: Observable<APIError> {
        return flatMap { actionError -> Observable<APIError> in
            switch actionError {
            case .underlyingError(let apiError as APIError):
                return .just(apiError)
            default:
                return .empty()
            }
        }
    }
}
