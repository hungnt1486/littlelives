//
//  APICall.swift
//  FUTA
//
//  Created by mac on 19/05/2022.

//

import Foundation
import Alamofire
import RxSwift


protocol APICall {
    var domain: AppEnvironment.LittleLivesDomain { get }
    var path: String { get }
    var method: Alamofire.HTTPMethod { get }
    var paramEncoding: ParameterEncoding { get }
    var headers: HTTPHeaders { get }
    var urlParams: [String: Any]? { get }
    var interceptor: RequestInterceptor { get }
}

extension APICall {
    var baseUrl: String {
        domain.description + path
    }
    
    var paramEncoding: ParameterEncoding {
        URLEncoding(destination: .queryString)
    }
    
    var urlParams: [String: Any]? {
        [String: Any]()
    }
    
    var interceptor: RequestInterceptor {
        HttpInterceptor()
    }
}

enum APIError: Swift.Error {
    case invalidURL
    case serviceReponseError(ErrorInfo)
    case unexpectedResponse
    case decodeError
    case apiReponseError(ErrorInfo)
    case custom(String)
    case networkDown
}

extension APIError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .invalidURL: return "Invalid URL"
        case .serviceReponseError(let errorInfo): return "Unexpected HTTP code: \(errorInfo.status)"
        case .unexpectedResponse: return "unexpected_error"
        case .decodeError: return "Decode Error"
        case .apiReponseError(let errorInfo): return errorInfo.message
        case .custom(let errString): return errString
        case .networkDown: return "network_down"
        }
    }
    var errorType: APIError {
        return self
    }
}

typealias HTTPCode = Int

enum HttpRequestHeaders {
    enum ContentType {
        static let formUrlEncoded = "application/x-www-form-urlencoded"
        static let json = "application/json"
        static let patchJson = "application/json-patch+json"
    }
}

struct EmptyEntity: Codable, EmptyResponse {
    static func emptyValue() -> EmptyEntity {
        return EmptyEntity.init()
    }
}

extension APICall {
    func call() -> Observable<Void> {
        return Observable<Void>.create { observer in
            let request = AF.request(self.baseUrl,
                                     method: method,
                                     parameters: urlParams,
                                     encoding: paramEncoding,
                                     headers: headers
                                     //interceptor: interceptor
            )
            request.validate()
                .response { (response) in
                    
                    switch response.result {
                    case .success(let value):
                        guard let value = value else {
                            logAPIError(result: APIError.unexpectedResponse.localizedDescription, path: self.baseUrl, method: method, params: urlParams, header: headers, cURL: request.cURLDescription())
                            observer.onError(APIError.unexpectedResponse)
                            return
                        }
                        logAPISuccess(result: (String(data: value, encoding: .utf8) ?? ""), path: baseUrl, method: method, params: urlParams, header: headers, cURL: request.cURLDescription())

                        
                        resultHandlder(data: value, observer: observer)
                    case .failure(let error):
                        logAPIError(result: error.localizedDescription, path: self.baseUrl, method: method, params: urlParams, header: headers, cURL: request.cURLDescription())
                        errorHandler(response: response, error: error, observer: observer)
                    }
                }
            return Disposables.create() {
                request.cancel()
            }
            
        }
    }
    
    
    func call<T: Decodable>() -> Observable<T> {
        return Observable<T>.create { observer in
            
            let request = AF.request(self.baseUrl,
                                     method: method,
                                     parameters: urlParams,
                                     encoding: paramEncoding,
                                     headers: headers
                                     //interceptor: interceptor
            )
            request.validate()
                .response { (response) in
                    
                    switch response.result {
                    case .success(let value):
                        guard let value = value else {
                            logAPIError(result: APIError.unexpectedResponse.localizedDescription, path: self.baseUrl, method: method, params: urlParams, header: headers, cURL: request.cURLDescription())
                            observer.onError(APIError.unexpectedResponse)
                            return
                        }   
                        logAPISuccess(result: (String(data: value, encoding: .utf8) ?? ""), path: baseUrl, method: method, params: urlParams, header: headers, cURL: request.cURLDescription())
                        
                        resultHandlder(data: value, observer: observer)
                    case .failure(let error):
                        logAPIError(result: error.localizedDescription, path: self.baseUrl, method: method, params: urlParams, header: headers, cURL: request.cURLDescription())
                        errorHandler(response: response, error: error, observer: observer)
                    }
                }
            return Disposables.create() {
                request.cancel()
            }
        }
    }
    
//    func calls<T: Decodable>() -> Observable<[T]> {
//        return Observable<[T]>.create { observer in
//            
//            let request = AF.request(self.baseUrl,
//                                     method: method,
//                                     parameters: urlParams,
//                                     encoding: paramEncoding,
//                                     headers: headers
//                                     //interceptor: interceptor
//            )
//            request.validate()
//                .response { (response) in
//                    
//                    switch response.result {
//                    case .success(let value):
//                        guard let value = value else {
//                            logAPIError(result: APIError.unexpectedResponse.localizedDescription, path: self.baseUrl, method: method, params: urlParams, header: headers, cURL: request.cURLDescription())
//                            observer.onError(APIError.unexpectedResponse)
//                            return
//                        }
//                        logAPISuccess(result: (String(data: value, encoding: .utf8) ?? ""), path: baseUrl, method: method, params: urlParams, header: headers, cURL: request.cURLDescription())
//                        
//                        resultHandlder(data: value, observer: observer)
//                    case .failure(let error):
//                        logAPIError(result: error.localizedDescription, path: self.baseUrl, method: method, params: urlParams, header: headers, cURL: request.cURLDescription())
//                        errorHandler(response: response, error: error, observer: observer)
//                    }
//                }
//            return Disposables.create() {
//                request.cancel()
//            }
//        }
//    }
//    
    private func resultHandlder(data: Data, observer: AnyObserver<Void>) {
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
            if let status = json?["status"] as? Int, status != 200 {
                observer.onError(APIError.apiReponseError(ErrorInfo(data: data, statusCode: status)))
                return
            }
            
            observer.on(.next(()))
            observer.on(.completed)
            
        } catch {
            #if DEBUG
            observer.onError(APIError.decodeError)
            #else
            observer.onError(APIError.unexpectedResponse)
            #endif
        }
    }
    
    private func resultHandlder<T: Decodable>(data: Data, observer: AnyObserver<T>) {
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
            if let status = json?["status"] as? Int, status != 200 {
                observer.onError(APIError.apiReponseError(ErrorInfo(data: data, statusCode: status)))
                return
            }
            
            let responseData = try JSONDecoder().decode(WrapResponse<T>.self, from: data)
            let status = responseData.status
            if status != "ok" {
                //todo
//                if let message = responseData.message {
//                    observer.onError(APIError.custom(message))
//                } else {
//                    observer.onError(APIError.unexpectedResponse)
//                }
            } else {
                let data = responseData.articles
                observer.on(.next(data))
                observer.on(.completed)
            }
        } catch {
            #if DEBUG
            observer.onError(APIError.decodeError)
            #else
            observer.onError(APIError.unexpectedResponse)
            #endif
        }
    }
    
    private func errorHandler<Output>(response: AFDataResponse<Data?>, error: AFError, observer: AnyObserver<Output>) {

        if let code = (error.underlyingError as? URLError)?.errorCode,
           code == NSURLErrorNotConnectedToInternet || code == NSURLErrorBadServerResponse {
            observer.on(.error(APIError.networkDown))
            return
        }
        guard let code = error.responseCode else {
            observer.on(.error(APIError.unexpectedResponse))
            return
        }
        
        observer.onError(APIError.apiReponseError(ErrorInfo(data: response.data, statusCode: code)))
    }
    
    private func logAPISuccess(result: String,path: String, method: Alamofire.HTTPMethod, params: Parameters?, header: HTTPHeaders?, cURL: String) {
        
        
        let text = """
                    ======================
                    ℹ️ Input:
                    \(geDescription(path: path, method: method, params: params, header: headers))
                    
                    🔥 Response:
                    \(result)
                    ======================
                    
                    ⚠️ CURL:
                    \(cURL)
                    """
        print(text)
       
    }
    
    private func logAPIError(result: String,path: String, method: Alamofire.HTTPMethod, params: Parameters?, header: HTTPHeaders?, cURL: String) {
   
        let text = """
                       ======================
                       ℹ️ Input:
                       \(geDescription(path: path, method: method, params: params, header: headers))
                       
                       🐞 Error::
                       \(result)
                       ======================
                       
                       ⚠️ CURL:
                       \(cURL )
                       """
        print(text)
        
    }
    
    
    private func geDescription(path: String, method: Alamofire.HTTPMethod, params: Parameters?, header: HTTPHeaders? ) -> String {
        return """
                   path: \(path)
                   method: \(method)
                   params: \(params ?? [:])
                   header: \(header ?? [:])
               """
    }
}
