//
//  HttpInterceptor.swift
//  FUTA
//
//  Created by mac on 19/05/2022.

//

import Foundation
import Alamofire
import RxSwift

class HttpInterceptor: RequestInterceptor {
    let retryLimit = 2
    let retryDelay: TimeInterval = 10
    var isPersonalAccount = true
    var ignoreRetry = false
    var cancellationBag = DisposeBag()
    
    func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest, Error>) -> Void) {
        
//        var urlRequest = urlRequest
//        let accessToken = isPersonalAccount ? AccountHelper.getAccessToken() : AccountHelper.getOrgAccessToken()
//        if let accessToken = accessToken {
//            urlRequest.headers.add(HTTPHeader.authorization(bearerToken: accessToken))
//        }
        completion(.success(urlRequest))
         
    }
    
    func retry(_ request: Request, for session: Session, dueTo error: Error, completion: @escaping (RetryResult) -> Void) {
        let response = request.task?.response as? HTTPURLResponse
        if let statusCode = response?.statusCode,
           statusCode == 401,
           request.retryCount < retryLimit, !ignoreRetry {
            refreshToken(completion: completion)
        } else {
            return completion(.doNotRetry)
        }
    }
    
    private func refreshToken(completion: @escaping (RetryResult) -> Void) {
        /*
        let authRepo = AuthenticationRepositoryImpl(authService: AuthServiceImpl())
        let refreshToken = isPersonalAccount ? authRepo.refreshToken() : authRepo.refreshOrgToken()
        refreshToken.subscribe(on: DispatchQueue.global()).sinkToResult { (result) in
            switch result {
            case .success:
                return completion(.retry)
            default:
                return completion(.doNotRetry)
            }
        }.store(in: cancellationBag)
         */
    }
}
